import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DetailsPage } from '../details/details.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  Articles : any[];

  constructor(public navCtrl: NavController) {
    this.Articles = [
      {nom : 'Télévision', prix : 145, details : 'Lorem ipsum de Télévision. Lorem impsum dolor sit amet.'},
      {nom : 'Téléphone', prix : 89, details : 'Lorem ipsum de Téléphone. Lorem impsum dolor sit amet.'},
      {nom : 'Sac à dos', prix : 11, details : 'Lorem ipsum de Sac à dos. Lorem impsum dolor sit amet.'},
      {nom : 'PC', prix : 350, details : 'Lorem ipsum de PC. Lorem impsum dolor sit amet.'},
    ]
  }

  showDetails(data : any) : void {
    this.navCtrl.push(DetailsPage, {data : data});
  }
  showData(data : any) : void {
    console.log('data is', data);
  }

}
