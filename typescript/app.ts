console.log('Hello');

// déclarer un type de variable string
var monNom : string = 'Julien';
console.log('1ère valeur de nomNom', monNom);

// boolean
// monNom = true; erreur car nomNom doit etre un string
// string, number, boolean, undefined, null, void 

// Le type any : elle pourra contenir n'importe quel type de variable
var x : any = 'Salut !';
console.log('1ère valeur de x', x);
x = false;
console.log('2è valeur de x', x);
x = 44;
console.log('3è valeur de x', x);

// le type tuple : tableau qui aura uniquement 2 items qui seront de type
var y : [string, boolean] = ['Julien', true];
// y = [true, 'Julien']; // erreurs car l'ordre n'est pas bon

// type multiple (union type)
var z : string | number;
z = 5;
console.log('1ère valeur de z', z);
z = 'Coucou';
console.log('2è valeur de z', z);

// object comme type (object as type)
var data : {
    nom : string,
    age : number,
    maried : boolean,
}
data = {
    nom : 'Fanny',
    age : 24,
    maried : false,
}
// data = {
//     nom : 5, // erreur car ce n'est pas un string
//     age : 24,
//     maried : false,
// }

// functions as type
// var myFx : (arg1 : x, arg2 : y, arg3 : z) => b; (return un type de valeur)
var myFx : (nom : string, age : number) => number;
myFx = function(nom, age) {
    return age*2;
}
console.log('Ma fonction est', myFx('Fanny', 24));

// Custom type
// Formule :
// type nomType = x
type myFunctions = (nom : string, age : number) => number;
var myFunction2 : myFunctions = function(nom, age) : number {
    return age*5;
};
console.log('Ma 2è function est', myFunction2('Marc', 10));

// Array
var tab1 : number[]; // tableau de nombres
// tab1 = ['Fanny', 5, 12, 3, 54]; // erreur car 'Fanny' n'est pas un nombre
tab1 = [5, 12, 3, 54];
var tab2 : (number | string)[]; // peut contenir soit des nombres, soit des strings
tab2 = ['Fanny', 5, 12, 3, 54];

// Interfaces
// Formule :
// interface nomInterface {
//     propertiesObligatoires : x,
//     propertiesOptionnelles ? : y,
//     methods(arguments et leurs types s'il y en a) : x,
// }
interface status {
    nom : string;
    age : number;
    maried : boolean;
    nbChildren ?: number;
    salary(age : number, nbChildren : number) : number;
}

var myStatus : status = {
    nom : 'Fanny',
    age : 24,
    maried : false,
    salary : function(x : number, y : number) : number {
        return x*y;
    }
}

interface status2 extends status {
    adress : string;
}

var myStatus2 : status2 = {
    adress : 'mon adresse',
    nom : 'Toto',
    age : 32,
    maried : true,
    nbChildren : 2,
    salary : function(x : number, y : number) : number {
        return x*y;
    }
}

// Arrow functions
var Fx1 = function (value){
    console.log(value);
}
// en JS

var Fx2 = (value : any) => console.log(value);
Fx1('1ère fonction Fx1 : non fléchée');
Fx2('2è fonction Fx2 : fléchée');

// 1. Functions with many arguments 
// (liste d'arguments avec leurs types) => {
//     value = value + 'salut';
//     console.log(value);
// }
var Fx3 = (x : number, y : number) : number => {
    x = x + y;
    return x;
}

// 2. Functions with one argument
var Fx4 = x => {
    console.log(x);
}

// 3. Functions with 0 argumen : anonymes
var Fx5 = () : void => {
    console.log('Aucun argument');
}

// Les Classes
// class nomClass { }
// Accès : private, protected, public
class User {
    public name : string;
    public email : string;
    public password : string;
    protected isLoggedIn : boolean; // protected ne peut être utilisé que dans cette classe ou une class enfant
    private age : number;

    constructor(name : string, email : string, password : string) {
        console.log('Je suis le constructeur');
        this.createUser(name, email, password);
    }
    createUser(name : string, email : string, password : string) : void {
        this.name = name;   
        this.email = email;   
        this.password = password;   
    }

    setAge(age : number) : void {
        this.age = age;
    }
    getAge() : any {
        return this.age;
    }
}
let myUser = new User('Fanny', 'test@test.fr', 'root');
console.log('myUser est', myUser);
console.log(myUser.name);

myUser.setAge(24);
console.log("L'age de", myUser.name, "est", myUser.getAge());
