console.log('Hello');
// déclarer un type de variable string
var monNom = 'Julien';
console.log('1ère valeur de nomNom', monNom);
// boolean
// monNom = true; erreur car nomNom doit etre un string
// string, number, boolean, undefined, null, void 
// Le type any : elle pourra contenir n'importe quel type de variable
var x = 'Salut !';
console.log('1ère valeur de x', x);
x = false;
console.log('2è valeur de x', x);
x = 44;
console.log('3è valeur de x', x);
// le type tuple : tableau qui aura uniquement 2 items qui seront de type
var y = ['Julien', true];
// y = [true, 'Julien']; // erreurs car l'ordre n'est pas bon
// type multiple (union type)
var z;
z = 5;
console.log('1ère valeur de z', z);
z = 'Coucou';
console.log('2è valeur de z', z);
// object comme type (object as type)
var data;
data = {
    nom: 'Fanny',
    age: 24,
    maried: false
};
// data = {
//     nom : 5, // erreur car ce n'est pas un string
//     age : 24,
//     maried : false,
// }
// functions as type
// var myFx : (arg1 : x, arg2 : y, arg3 : z) => b; (return un type de valeur)
var myFx;
myFx = function (nom, age) {
    return age * 2;
};
console.log('Ma fonction est', myFx('Fanny', 24));
var myFunction2 = function (nom, age) {
    return age * 5;
};
console.log('Ma 2è function est', myFunction2('Marc', 10));
// Array
var tab1; // tableau de nombres
// tab1 = ['Fanny', 5, 12, 3, 54]; // erreur car 'Fanny' n'est pas un nombre
tab1 = [5, 12, 3, 54];
var tab2; // peut contenir soit des nombres, soit des strings
tab2 = ['Fanny', 5, 12, 3, 54];
var myStatus = {
    nom: 'Fanny',
    age: 24,
    maried: false,
    salary: function (x, y) {
        return x * y;
    }
};
var myStatus2 = {
    adress: 'mon adresse',
    nom: 'Toto',
    age: 32,
    maried: true,
    nbChildren: 2,
    salary: function (x, y) {
        return x * y;
    }
};
// Arrow functions
var Fx1 = function (value) {
    console.log(value);
};
// en JS
var Fx2 = function (value) { return console.log(value); };
Fx1('1ère fonction Fx1 : non fléchée');
Fx2('2è fonction Fx2 : fléchée');
// 1. Functions with many arguments 
// (liste d'arguments avec leurs types) => {
//     value = value + 'salut';
//     console.log(value);
// }
var Fx3 = function (x, y) {
    x = x + y;
    return x;
};
// 2. Functions with one argument
var Fx4 = function (x) {
    console.log(x);
};
// 3. Functions with 0 argumen : anonymes
var Fx5 = function () {
    console.log('Aucun argument');
};
// Les Classes
// class nomClass { }
// Accès : private, protected, public
var User = /** @class */ (function () {
    function User(name, email, password) {
        console.log('Je suis le constructeur');
        this.createUser(name, email, password);
    }
    User.prototype.createUser = function (name, email, password) {
        this.name = name;
        this.email = email;
        this.password = password;
    };
    User.prototype.setAge = function (age) {
        this.age = age;
    };
    User.prototype.getAge = function () {
        return this.age;
    };
    return User;
}());
var myUser = new User('Fanny', 'test@test.fr', 'root');
console.log('myUser est', myUser);
console.log(myUser.name);
myUser.setAge(24);
console.log("L'age de", myUser.name, "est", myUser.getAge());
