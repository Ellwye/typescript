import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
 
@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  addition : number = 5 + 10;
  x : number;
  y : number;
  isLoggedIn : boolean = false;
  myName : string;
  myList : any[];
  connectedAt : Date;

  constructor(public http : HttpClient) { 
    this.x = 7;
    this.y = 3;
    // this.login();
    this.myList = [
      {
        name : 'Meteor',
        disponible : true,
      },
      {
        name : 'Electron',
        disponible : false,
      },
      {
        name : 'Webpack',
        disponible : false,
      },
      {
        name : 'React',
        disponible : true,
      },
      {
        name : 'RxJS',
        disponible : true,
      },
    ];
    this.http.get('https://jsonplaceholder.typicode.com/users').subscribe((data) => {
      console.log('data is', data);
    });
  }

  ngOnInit(): void {
  }

  multiplication() : number {
    return this.x * this.y;
  }
  login() {
    setTimeout(() => {
      this.isLoggedIn = true;
      alert('Vous êtes maintenant connecté que ' + this.myName);
      this.connectedAt = new Date();
    }, 200)
  }
  getStatus(){
    if(this.isLoggedIn){
      return 'green';
    } else {
      return 'red';
    }
  }
}
